const validTypes = ["Plantes", "Poisson", "Dragon", "Acier", "Psy", "Electrick", "Feu", "Eau", "Insecte", "Fée", "Normal"]
const validSpecificity = ["Ordinaire", "Légendaire", "Fabuleux", "Inconnu", "Ultra-Chimère"]
const validOrigin = ["Kanto", "Johto", "Hoenn", "Sinnoh", "Unys", "Kalos", "Alola", "Galar","Hisui"]
module.exports = (sequelize, DataTypes) => {
    // Ici 'Pokemon' est le nom de la table de base de donnée
return sequelize.define('Pokemon', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique:{
            msg:"The name entered already exists"
            },
        validate:{
            notEmpty:{msg:"This field must not be empty"},
            notNull:{msg:"The name of the pokemon must be filled in"}
        }
    
    },
    origin: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            isTypesValid(value) {
                value.split(',').forEach((origin) => {
                    if(!validOrigin.includes(origin)){
                        console.error("Invalid")
                    // throw new Error(`Le type d'un pokémon doit appartenir à la liste suivante: ${validTypes}`);
                    }
                })
                
                }
            }
    },
    specificity: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            isTypesValid(value) {
                value.split(',').forEach((specificity) => {
                    if(!validSpecificity.includes(specificity)){
                        console.error("Invalid")
                    // throw new Error(`Le type d'un pokémon doit appartenir à la liste suivante: ${validTypes}`);
                    }
                })
                
                }
            }
    },

    hp: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate:{
            isInt:{msg:"Use only whole numbers for hit points."},
            notNull:{msg:"Life points are a required property."},
            max: {
                args: [9999],
                msg: "life points must be greater than or equal to 9999"
            },
            min: {
                args: [10],
                msg: "life points must be greater than or equal to 10"
            }
        }

    },

    cp: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate:{
            isInt:{msg:"Use only whole numbers for damage points."},
            notNull:{msg:"Damage points are a required property."},
            max: {
                args: [99],
                msg: "life points must be greater than or equal to 99"
            },
            min: {
                args: [0],
                msg: "life points must be greater than or equal to 0"
            }
    
            }
    
    },

    picture: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            isUrl: { msg: "Use only a valid url for the image."},
            notNull: { msg: "Picture is a required property"}
        }

    },
    types: {
        type: DataTypes.STRING,
        allowNull: false,
        get() {
            return this.getDataValue('types').split(',')
            },
        set(types) {
            this.setDataValue('types', types.join())
            },        
            validate: {
                isTypesValid(value) {
                    if(!value){
                        throw new Error("A pokémon must have at least one type.")
                    }
                    if(!value.split(',').length > 3){
                        throw new Error("A pokémon cannot have more than three types")
                    }
                    value.split(',').forEach((type) => {
                        if(!validTypes.includes(type)){
                            console.error("Invalid")
                        // throw new Error(`Le type d'un pokémon doit appartenir à la liste suivante: ${validTypes}`);
                        }
                    })
                    
                    }
                }
        
    }
    }, {
        timestamps: true,
        createdAt: 'created',
        updatedAt: false
    })
}