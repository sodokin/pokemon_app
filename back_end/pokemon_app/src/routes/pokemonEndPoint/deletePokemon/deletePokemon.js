const { Pokemon } = require('../../../db/sequelize');
const auth = require('../../../auth/auth')
    
    module.exports = (app) => {
    app.delete('/api/deletepokemons/:id',auth, (req, res) => {
        Pokemon.findByPk(req.params.id).then(pokemon => {
        if (pokemon === null) {
            const message = "Pokemon requested does not exist. Try again with a different ID"
            return res.status(404).json({message})
        }
        const pokemonDeleted = pokemon;
        return Pokemon.destroy({
            where: { id: pokemon.id }
        })
        .then(_ => {
            const message = `The pokémon with the ID n°${pokemonDeleted.id} has been deleted.`
            res.json({message, data: pokemonDeleted })
        })
        })
        .catch(error => {
            const message = `The pokemon could not be deleted. Please try again in a few moments.`
            res.status(500).json({message, data: error})
        })
    })
}
