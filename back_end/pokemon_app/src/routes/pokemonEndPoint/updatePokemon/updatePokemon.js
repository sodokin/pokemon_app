const { Pokemon } = require('../../../db/sequelize')
const { ValidationError } = require("sequelize")
const auth = require('../../../auth/auth')

module.exports = (app) => {
    app.put('/api/updatepokemons/:id',auth, (req, res) => {
        const id = req.params.id
        Pokemon.update(req.body, {
        where: { id: id }
        })
        .then(_ => {
        return Pokemon.findByPk(id).then(pokemon => {
            if (pokemon === null){
                const message = "Pokemon requested does not exist. Try again with a different Id"
                    return res.status(400).json({message})
            }
            const message = `The pokemon ${pokemon.name} has been succesfully modified.`
            res.json({message, data: pokemon })
        })
        })
        .catch(error => {
        if(error instanceof ValidationError) {
            return res.status(400).json({message: error.message, data: error})
        }
        if(error instanceof UniqueConstraintError){
            return res.status(400).json({message: error.message, data: error})
        }

        const message = `The pokemon could not be modified. Try again in a few moments.`
        res.status(500).json({message, data:error})
        
        })
    })
}







