const { Pokemon } = require('../../../db/sequelize')
const { Op } = require('sequelize')
const auth = require('../../../auth/auth')



module.exports = (app) => {
    app.get('/api/pokemons',auth, (req, res) => {
        if(req.query.name) {
            const name = req.query.name
            const limit = parseInt(req.query.limit) || 5
        if(name.length < 2){
            const message = "The search must contain at least 2 characters."
            return res.status(400).json({message})
        }
        return Pokemon.findAndCountAll({ 
            where: { 
                name: { // Ici name représente la propriété "name" du modèle de l'api
                    [Op.like]: `%${name}%` // Tandis qu'ici, name repésente le critère de recherche
                } 
            },
            order: ['name'], //Ici name représente encore la propriété du modele de l'api. On peut donc ordonner le résultat de la recherche sur d'autres propriétés du modele selon les besoins
            limit: limit // Ici on met en place une limite dynamique
        })
        .then(({count, rows}) => {
            const message = `There are ${count} pokemons qui correspondent à la recherche: ${name}`
            res.json({message, data: rows})
        })
        }
        Pokemon.findAll({order: ['name']})
        .then(pokemons => {
            const message = 'The list of pokemons has been recovered.'
            res.json({ message, data: pokemons })
        })
        .catch(error => {
            const message = "The list of pokemons could not be retrieved. Please try again in a few moments. "
            res.status(500).json({message, data:error})
        })
    })
}




    // module.exports = (app) => {
    //     app.get('/api/pokemons',(req, res) => {
    //         Pokemon.findAll()
    //             .then(pokemons => {
    //                 const message = 'La liste des pokémons a bien été récupérée.'
    //                 res.json({ message, data: pokemons })
    //         })
    //         .catch(error => {
    //             const message = "La liste des pokemons n'a pas pu être récupérée. Réessayez dans quelques instants. "
    //             res.status(500).json({message, data:error})
    //         })
    // })
    // }
