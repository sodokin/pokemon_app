const { Pokemon } = require('../../../db/sequelize');
const { ValidationError, UniqueConstraintError } = require('sequelize');
const auth = require('../../../auth/auth')

module.exports = (app) => {
    app.post('/api/addpokemons', auth, (req, res) => {
        Pokemon.create(req.body)
            .then(pokemon => {
                const message = `The pokemon ${req.body.name} has been succefully created.`
                res.json({ message, data: pokemon })
            })
            .catch(error => {
                if(error instanceof ValidationError) {
                    return res.status(400).json({message: error.message, data: error})
                }
                if(error instanceof UniqueConstraintError){
                    return res.status(400).json({message: error.message, data: error})
                }
                const message = `The pokémon could not be added. Please try again in a few moments.`
                res.status(500).json({message, data: error})
        })
    })
}


