const { User } = require('../../../db/sequelize');
const auth = require('../../../auth/auth') 


module.exports = (app) => {
    app.delete("/api/pokemon/deleteuser/:id", auth, (req, res) => {
        User.findByPk(req.params.id).then( user => {
            if (user === null) {
                const message = `The user you want to delete does not exist. Try again with another user.`
                return res.status(404).json({ message })
            }
            const userDeleted = user;
            return User.destroy({
                where: { id: user.id}
            })
            .then(_ => {
                const message = `The user identified with the number ${userDeleted.id}has been deleted .`
                res.json({message, data: userDeleted})
            })
        })
        .catch(error => {
            const message = `The user could not be deleted. Please try again in a few moments.`
            res.status(500).json({ message, data: error})
        })

    })
}