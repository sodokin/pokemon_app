const { Sequelize, DataTypes } = require('sequelize')
const PokemonsModel = require('../models/pokemonsModel')
const UsersModel = require('../models/userModel')
// const bcrypt = require('bcrypt');


const sequelize = new Sequelize('angularpokeappdb', 'koudjo', 'koudjo89', {
    host: 'localhost',
        dialect: 'postgres',
        dialectOptions: {
            timezone: 'Etc/GMT-2'
        },
        logging: false
    })
    
sequelize.authenticate()
    .then(_ => console.log("the connection to the database has been established"))
    .catch(error => console.error(`Unable to connect to the database ${error}`))

const User = UsersModel(sequelize, DataTypes)
const Pokemon = PokemonsModel(sequelize, DataTypes)
// sequelize.sync({force:true})
//     .then(_ => console.log(`La base de donnée a bien été synchronisée !`))

const initDb = async() => {
    if (User.firstname !== undefined && User.lastname && User.email && User.password !== undefined) {
        return await sequelize.sync().then(_ => {
            User.create({
                firstname: firstname,
                lastname: lastname,
                email: email,
                password: password
            })
            .then(user => console.log(user.toJSON()))
            .then(_ => console.log("The user has been created !"));
    
        })
    }
    
    if(Pokemon.name !== undefined && Pokemon.origin !== undefined && Pokemon.specificity !== undefined && Pokemon.hp !== undefined  && Pokemon.cp !== undefined  && Pokemon.types !== undefined) {
        return await sequelize.sync().then(_ => {
            Pokemon.create({
                name: name,
                origin: origin,
                specificity: specificity,
                hp: hp,
                cp: cp,
                types: types
            }).then(pokemon => console.log(`The pokemon has been succesfully created ${pokemon.toJSON()} `))
        })
    }



    // const initDb = () => {
    //     return sequelize.sync({force: true}).then(_ => {
    //         pokemons.map(pokemon => {
    //             Pokemon.create({
    //             name: pokemon.name,    
    //             hp: pokemon.hp,
    //             cp: pokemon.cp,
    //             picture: pokemon.picture,
    //             types: pokemon.types
    //             }).then(pokemon => console.log(pokemon.toJSON()))
    //     })
    
    //     bcrypt.hash('utilisateur', 10)
    //     .then(hash => User.create({username: "Bob", password: hash}))
    //     .then(user => console.log(user.toJSON()))
    //     console.log('La base de donnée a bien été initialisée !')
    //     })
    // }


}

module.exports = {
    initDb, Pokemon, User
}
