const express =  require('express');
const bodyParser = require('body-parser')
const morgan = require('morgan')
const sequelize = require('./src/db/sequelize');
const cors = require('cors');


const app = express();
const port = 3015

app
    .use(morgan("dev"))
    .use(bodyParser.json())
    .use(cors())


sequelize.initDb()
// User Api Url
require('./src/routes/userEndPoint/userLog/register')(app)
require('./src/routes/userEndPoint/userLog/login')(app)
require('./src/routes/userEndPoint/updateUser/updateUser')(app)
require('./src/routes/userEndPoint/userList/showUserList')(app)
require('./src/routes/userEndPoint/onUser/showOneUser')(app)
require('./src/routes/userEndPoint/deleteUser/deleteUser')(app)


// Pokemon app Url
require('./src/routes/pokemonEndPoint/pokemonList/pokemonList')(app)
require('./src/routes/pokemonEndPoint/createPokemon/createPokemon')(app)
require('./src/routes/pokemonEndPoint/onePokemon/showOnePokemon')(app)
require('./src/routes/pokemonEndPoint/updatePokemon/updatePokemon')(app)
require('./src/routes/pokemonEndPoint/deletePokemon/deletePokemon')(app)



app.use(({res}) => {
    const message = "Unable to find the requested resource! You can try another URL."
    res.status(404).json({message})
    }
)

app.listen(port, () => console.log(`listening on port : http://localhost:${port}`));
