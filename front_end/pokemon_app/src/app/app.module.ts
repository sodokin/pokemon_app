import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PokemonmanagementModule } from './pokemonmanagement/pokemonmanagement.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AppauthenticationModule } from './appauthentication/appauthentication.module';
import { TokenInterceptorProvider } from './appauthentication/token.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppauthenticationModule,
    PokemonmanagementModule,
    AppRoutingModule
  ],
  providers: [TokenInterceptorProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
