export interface Pokemon {
    id: number;
    name: string;
    origin: string;
    specificity: string;
    hp: number;
    cp: number;
    picture: string;
    types: Array<string>;
    created: Date;
}

export interface Apidata {
    message: string;
    data: Array<Pokemon>;
}

export interface PokemonData {
    message: string;
    data: Pokemon;
}
// An object to create new pokemon
export class SaveNewPokemon {
    id: number;
    name: string;
    origin: string;
    specificity: string;
    hp: number;
    cp: number;
    picture: string;
    types: Array<string>;
    created: Date;

    constructor(
        name: string = 'Entrez un nom ...',
        origin: string = 'De quelle région provient votre pokémon?',
        specificity: string = 'De qelle catégorie est votre pokémon?',
        hp: number = 0,
        cp: number = 0,
        picture: string = 'http://assets.pokemon.com/assets/cms2/img/pokedex/detail/xxx.png',
        types: string[] = ['Normal'],
        created: Date = new Date()
    ) {
        this.name = name;
        this.origin = origin;
        this.specificity = specificity;
        this.hp = hp;
        this.cp = cp;
        this.picture = picture;
        this.types = types;
        this.created = created;
    }
}