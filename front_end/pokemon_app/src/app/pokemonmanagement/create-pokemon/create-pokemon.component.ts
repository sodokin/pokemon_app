import { Component, OnInit } from '@angular/core';
import { SaveNewPokemon } from '../pokemon-model';
// import { PokemonService } from '../pokemon.service';
// import { Router } from '@angular/router';

@Component({
  selector: 'app-create-pokemon',
  templateUrl: './create-pokemon.component.html',
  styles: [
  ]
})
export class CreatePokemonComponent implements OnInit {
  newPokemon: SaveNewPokemon

  constructor(
    // private pokemonService: PokemonService,
    // private router: Router 
  ){}

  ngOnInit() {
    this.addPokemon()
  }

  addPokemon(){
    this.newPokemon = new SaveNewPokemon()
    // this.pokemonService.createPokemon(this.pokemon)
  }

}
