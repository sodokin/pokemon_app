import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PokemonTypeColorPipe } from './pokemon-type-color.pipe';
import { PokemonListComponent } from './pokemon-list/pokemon-list.component';
import { OnePokemonDetailComponent } from './one-pokemon-detail/one-pokemon-detail.component';
import { RouterModule, Routes } from '@angular/router';
import { PokemonService } from './pokemon.service';
import { BorderCardDirective } from './border-card.directive';
import { UpdatePokemonComponent } from './update-pokemon/update-pokemon.component';
import { CreatePokemonComponent } from './create-pokemon/create-pokemon.component';
import { LoaderComponent } from './loader/loader.component';
import { SearchPokemonComponent } from './search-pokemon/search-pokemon.component';
import { PokemonFormComponent } from './pokemon-form/pokemon-form.component';
import { FormsModule } from '@angular/forms';
import { AuthGuard } from '../auth.guard';


const PokemonManagementRoutes: Routes = [
  {path: 'pokemons', component: PokemonListComponent, canActivate: [AuthGuard]},
  {path: 'pokemons/create', component: CreatePokemonComponent, canActivate: [AuthGuard]},
  {path: 'pokemons/details/:id', component: OnePokemonDetailComponent, canActivate: [AuthGuard]},
  {path: 'pokemon/update/:id', component: UpdatePokemonComponent, canActivate: [AuthGuard]},
]

@NgModule({
  declarations: [
    PokemonListComponent,
    OnePokemonDetailComponent,
    PokemonTypeColorPipe,
    BorderCardDirective,
    UpdatePokemonComponent,
    CreatePokemonComponent,
    LoaderComponent,
    SearchPokemonComponent,
    PokemonFormComponent
  ],
  
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(PokemonManagementRoutes)
  ],
  providers: [PokemonService]
})
export class PokemonmanagementModule { }
