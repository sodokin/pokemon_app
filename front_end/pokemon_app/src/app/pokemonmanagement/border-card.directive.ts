import { Directive, ElementRef, HostListener,Input } from '@angular/core';

@Directive({
  selector: '[appBorderCard]'
})
export class BorderCardDirective {

  private InitialColor: string = '#e5e9f0'
  private defaultColor: string = '#8fbcbb'
  private defaultHeight: number = 180

  constructor(private cardElement: ElementRef) { 
    this.setBorder(this.InitialColor)
    this.setHeight(this.defaultHeight)
  }

  @Input('appBorderCard') cardBorder: string


  @HostListener('mouseenter') onMouseEnter() {
    this.setBorder(this.defaultColor)
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.setBorder(this.InitialColor)
  }

  setBorder(cardBorderColor: string){
    this.cardElement.nativeElement.style.border = `4px solid ${cardBorderColor}`
  }

  setHeight(cardHeight: number){
    this.cardElement.nativeElement.style.height = `${cardHeight}px`
  }
}
