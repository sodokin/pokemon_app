import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../pokemon-model';
import { PokemonService } from '../pokemon.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-update-pokemon',
  templateUrl: './update-pokemon.component.html',
  styles: [
  ]
})
export class UpdatePokemonComponent implements OnInit{
  pokemon: Pokemon|undefined

  constructor(
    private route: ActivatedRoute, 
    private pokemonService: PokemonService
  ){}

  ngOnInit(): void {
    this.editPokemon()
  }

  editPokemon() {
    const pokeId: any = this.route.snapshot.paramMap.get('id')
    if(pokeId){
      this.pokemonService.getPokemonById(+pokeId).subscribe(datas => {
        console.log(datas.data)
        this.pokemon = datas.data
      })   
    } else {
      this.pokemon = undefined
    }

    
}
}