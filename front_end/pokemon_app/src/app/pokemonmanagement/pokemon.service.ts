import { Injectable } from '@angular/core';
import { Pokemon , Apidata, PokemonData} from '../pokemonmanagement/pokemon-model'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, of, tap } from 'rxjs';


@Injectable()
export class PokemonService {

  constructor(private http: HttpClient) { }


  createPokemon(pokemon: Pokemon): Observable<Pokemon> {
    return this.http.post<Pokemon>('http://localhost:3015/api/addpokemons', pokemon).pipe(
      tap((response) => this.log(response)),
      catchError((error) => this.handleError(error, null))
    )
  }


  getPokemonList(): Observable<Apidata> {
    // const httpOptions = {
    //   Headers: new HttpHeaders({'Content-type': 'application/json'})
    // }
    return this.http.get<Apidata>('http://localhost:3015/api/pokemons').pipe(
      tap((response) => this.log(response.data)),
      catchError((error) => this.handleError(error, undefined))
    )
  }

  getPokemonById(pokeId: number): Observable<PokemonData> {
    return this.http.get<PokemonData>(`http://localhost:3015/api/pokemondetails/${pokeId}`).pipe(
      tap((response) => this.log(response.data)),
      catchError((error) => this.handleError(error, undefined))
    )
  }

  updatePokemon(pokemon: Pokemon): Observable<Pokemon|undefined> {
    return this.http.put(`http://localhost:3015/api/updatepokemons/${pokemon.id}`,pokemon).pipe(
      tap((response) => this.log(response)),
      catchError((error) => this.handleError(error, null))
    )
  }


  deletePokemon(pokeId: number): Observable<null> {
    return this.http.delete(`http://localhost:3015/api/deletepokemons/${pokeId}`).pipe(
      tap((response) => this.log(response)),
      catchError((error) => this.handleError(error, null))
    )
  }


  private log(response: any) {
    console.log(response)
  }

  private handleError(error: Error, errorValue: any) {
    console.log(error)
    return of(errorValue)
  }

  getPokemonTypeList(): string[] {
    return ["Plante", "Feu", "Eau", "Insect", "Normal", "Electrick", "Poison", "Fée", "Vol", "Combat", "Psy", "Acier"]
  }

  getPokemonType(id: number) {

  }
}
