import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonService } from '../pokemon.service';
import { Pokemon, Apidata } from '../pokemon-model';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
})
export class PokemonListComponent implements OnInit {
  pokemonDatas: Apidata

  constructor(
    private router: Router,
    private pokemonService: PokemonService
  ) {}

  ngOnInit() {
    this.getList()
    // this.pokemonService.getPokemonList().subscribe(pokemolist => this.pokemonDatas = pokemolist)
  }

  getList() {
      this.pokemonService.getPokemonList().subscribe(data => {
        this.pokemonDatas = data
      })
  }

  displayPokemonDetail(pokemon: Pokemon){
    this.router.navigate([`/pokemons/details/${pokemon.id}`])
  }

}
