import { Component, Input, OnInit } from '@angular/core';
import { PokemonService } from '../pokemon.service';
import { Pokemon } from '../pokemon-model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pokemon-form',
  templateUrl: './pokemon-form.component.html',
  styleUrls: ['./pokemon-form.component.css']
})
export class PokemonFormComponent implements OnInit {
  @Input() currentPokemon: Pokemon
  pokemonsTypes: string[]
  isCreateUrl: boolean

  constructor(
    private router: Router,
    private pokemonServivce: PokemonService
    ){}

  ngOnInit() {
    this.getTypesList()
    this.createPokemonUrl()
  }
  
  getTypesList(){
    this.pokemonsTypes = this.pokemonServivce.getPokemonTypeList()
  }

  createPokemonUrl(){
    this.isCreateUrl = this.router.url.includes('create')
  }

  hasType(type: string): boolean {
    return this.currentPokemon.types.includes(type)
  }

  selectType($event: Event, type: string){
    const isChecked: boolean = ($event.target as HTMLInputElement).checked
    if(isChecked) {
      this.currentPokemon.types.push(type)
    } else {
      const uncheckedTypeIndex = this.currentPokemon.types.indexOf(type)
      this.currentPokemon.types.splice(uncheckedTypeIndex, 1)
    }
  }

  isTypesValid(type: string): boolean {
    if(this.currentPokemon.types.length == 1 && this.hasType(type)) {
      return false
    }

    if(this.currentPokemon.types.length > 2 && !this.hasType(type)){
      return false
    }
    return true

  }


  onSubmit(){
    if(this.isCreateUrl){
      this.pokemonServivce.createPokemon(this.currentPokemon).subscribe(() => this.router.navigate([`/pokemons`]) )
    } else {
      this.pokemonServivce.updatePokemon(this.currentPokemon).subscribe((pokemon) => {
        if(pokemon){
          this.router.navigate(['/pokemons/details/',this.currentPokemon.id])
        }
      })
      console.log('The form has been submited!')
      // this.router.navigate(['/pokemons/details/', this.currentPokemon.id])
    }
  }
}
