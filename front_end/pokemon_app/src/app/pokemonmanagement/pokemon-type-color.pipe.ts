import { Pipe, PipeTransform } from '@angular/core';
// Ici le pipe est instancié et un nom lui est attribué
@Pipe({
  name: 'pokemonTypeColor'
})
export class PokemonTypeColorPipe implements PipeTransform {

  // transform(value: unknown, ...args: unknown[]): unknown {
  //   return null;
  // }
  transform(type: string): string {
  
    let color: string;
  // Ici on utilise switch case pour definir une structure conditionnelle prennant en paramètre le type du pokemon, et la couleur
  // L'instruction break va permetre de ne pas lire le code restant dès lors que le case vaut true
    switch (type) {
      case 'Feu':
        color = 'red lighten-1';
        break;
      case 'Eau':
        color = 'blue lighten-1';
        break;
      case 'Plante':
        color = 'green lighten-1';
        break;
      case 'Insecte':
        color = 'brown lighten-1';
        break;
      case 'Normal':
        color = 'grey lighten-3';
        break;
      case 'Vol':
        color = 'blue lighten-3';
        break;
      case 'Poison':
        color = 'deep-purple accent-1';
        break;
      case 'Fée':
        color = 'pink lighten-4';
        break;
      case 'Psy':
        color = 'deep-purple darken-2';
        break;
      case 'Electrik':
        color = 'lime accent-1';
        break;
      case 'Combat':
        color = 'deep-orange';
        break;
      default:
        color = 'grey';
        break;
    }
  // Ici chip vients de materialize et permets d'afficher un petit rond auquel on attribut une couleur
    return 'chip ' + color;
  
  }

}
