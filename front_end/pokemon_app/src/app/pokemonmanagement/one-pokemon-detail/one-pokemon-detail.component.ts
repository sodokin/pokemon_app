import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pokemon, Apidata, PokemonData } from '../pokemon-model';
import { PokemonService } from '../pokemon.service';


@Component({
  selector: 'app-one-pokemon-detail',
  templateUrl: './one-pokemon-detail.component.html',
})
export class OnePokemonDetailComponent implements OnInit {
  pokemonDatas: PokemonData
  pokemon: Pokemon|undefined

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private pokemonService: PokemonService
  ) {

  }

  ngOnInit() {
    this.getPokeId()
  }

  getPokeId() {
    const pokeId: string|null = this.route.snapshot.paramMap.get('id')
    if(pokeId){
      this.pokemonService.getPokemonById(+pokeId).subscribe(thepokemon => {
        this.pokemon = thepokemon.data
        // this.pokemonDatas = thepokemon
        // console.log(thepokemon.data.id)
      })
    }
  }
  
  goToPokemonList() {
    this.router.navigate(['/pokemons'])
  }

  goToEditPokemon(pokemon: Pokemon) {
    this.router.navigate(['/pokemon/update/', pokemon.id])
  }

  deleteCurrentPokemon(pokemon: Pokemon) {
    this.pokemonService.deletePokemon(pokemon.id).subscribe(() => this.goToPokemonList() )
  }

}
