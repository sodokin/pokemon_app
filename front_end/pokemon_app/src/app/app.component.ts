import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from './appauthentication/token.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styles: []
})
export class AppComponent {

  constructor(
    private router: Router,
    private tokenService: TokenService
  ){

  }

  gotToUserList(){
    this.router.navigate(['pokemons/authentication/users'])
  }

  goToRegisterPage(){
    this.router.navigate(['pokemons/authentication/register'])
  }

  goToLoginPage(){
    this.router.navigate(['pokemons/authentication/login'])
  }

  logout(){
    this.tokenService.clearToken()
  }
}


