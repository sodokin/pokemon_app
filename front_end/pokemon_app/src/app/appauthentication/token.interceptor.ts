import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { Observable, catchError, throwError } from 'rxjs';
import { TokenService } from './token.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(
    private tokenService: TokenService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    console.log(request)

    const token = this.tokenService.getToken()
    // Here as we cannot modify the request, we must clone it in order to send the token in the header
    if(token !== null){
      let requestCloned = request.clone({
        headers: request.headers.set('Authorization', 'bearer '+token)
      })
      console.log(requestCloned)
      return next.handle(requestCloned).pipe(
        catchError((error) => {
          console.log(error)
          if(error.status === 401) {
            this.tokenService.clearExpiredToken()
          }
          return throwError('Session expired')
        })
      )
    }


    return next.handle(request);
  }
}
// Here, we configure the token interceptor in order to export it and insert it as a provider in the app.component.ts
export const TokenInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: TokenInterceptor,
  multi: true
}