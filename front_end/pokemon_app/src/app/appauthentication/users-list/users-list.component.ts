import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { User, UserApidata } from '../user-model';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styles: [
  ]
})
export class UsersListComponent implements OnInit {
  usersData: User[]

  constructor(
    private userService: UserService,
    private router: Router
    ){}

    ngOnInit() {
      this.getUserList()
    }

    getUserList(){
      this.userService.apiUserList().subscribe(datas => {
        this.usersData = datas.data
        console.log(datas.data)
      })
    }
}
