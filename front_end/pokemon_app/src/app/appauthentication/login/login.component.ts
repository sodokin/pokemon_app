import { Component, Input, OnInit } from '@angular/core';
import { LoginData, User } from '../user-model';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit{
  loguser: User
  constructor(
    private router: Router,
    private userService: UserService
  ){}

  ngOnInit() {
    this.verifyUser()
  }

  verifyUser(){
    this.loguser = new User()
  }
}



