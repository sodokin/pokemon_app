import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, of, tap } from 'rxjs';
import { User, UserApidata, LoginData } from './user-model';


@Injectable({
  providedIn: 'root'
})

export class UserService {
  token: string

  constructor(private http: HttpClient) { }

  createUser(user: User): Observable<User> {
    return this.http.post<User>('http://localhost:3015/api/pokemon/registerusers', user).pipe(
      tap((response) => this.log(response)),
      catchError((error) => this.handleError(error, null))
    )
  }

  apiUserList(): Observable<UserApidata> {
    // const httpOptions = {
    //   Headers: new HttpHeaders({'Content-type': 'application/json'})
    // }
    return this.http.get<UserApidata>('http://localhost:3015/api/pokemon/users').pipe(
      tap((response) => this.log(response)),
      catchError((error) => this.handleError(error, undefined))
    )
  }

  appLogin(loguser: User): Observable<LoginData> {
    return this.http.post<LoginData>('http://localhost:3015/api/pokemon/login', loguser).pipe(
      tap((response) => {
        this.token = response.token
        this.log(response)
      }),
      catchError((error) => this.handleError(error, undefined))
    )

  }


  private log(response: any) {
    console.log(response)
  }

  private handleError(error: Error, errorValue: any) {
    console.log(error)
    return of(errorValue)
  }


}
