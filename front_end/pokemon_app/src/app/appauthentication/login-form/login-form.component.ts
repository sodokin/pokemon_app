import { Component, Input, OnInit } from '@angular/core';
import { User } from '../user-model';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { TokenService } from '../token.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styles: [
  ]
})
export class LoginFormComponent implements OnInit {
  @Input() currentloguser: User
  
  constructor(
    private router: Router,
    private userService: UserService,
    private tokenService: TokenService
  ){}

  ngOnInit() {}

  onSubmit(){
    if(this.currentloguser){
      this.userService.appLogin(this.currentloguser).subscribe((userdata) => {
        this.tokenService.saveToken(userdata.token)
        
        if(userdata.token){
          this.router.navigate(['/pokemons'])
        }
      }
      )
    }
  }

}
