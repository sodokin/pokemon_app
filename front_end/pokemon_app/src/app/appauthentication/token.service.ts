import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(
    private router: Router
  ) { }

  saveToken(token: string){
    console.log(token)
    localStorage.setItem('token', token)
  }

  getToken(): string | null{
    return localStorage.getItem('token')
  }

  isLoggedIn(): boolean{
    const theToken = localStorage.getItem('token')
    console.log(theToken)
    return !! theToken
  }

  clearToken(){
    localStorage.removeItem('token')
    this.router.navigate(['pokemons/authentication/login'])
  }

  clearExpiredToken(){
    localStorage.removeItem('token')
    this.router.navigate(['pokemons/authentication/login'])

  }

}
