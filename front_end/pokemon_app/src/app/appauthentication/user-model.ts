// An object to create new pokemon
export class User {
    id: number;
    firstname: string;
    lastname: string;
    email: string;
    password: string;
    created: Date;

    constructor(
        firstname: string = 'Votre prénom ...',
        lastname: string = 'Votre nom ...',
        email: string = 'Votre adresse mail',
        password: string = 'mot de pass',
        created: Date = new Date()
    ) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.created = created;
    }
}

export interface UserApidata {
    message: string;
    data: Array<User>;
}

export interface LoginData {
    message: string;
    data: Array<User>;
    token: string;
}
