import { Component, Input, OnInit } from '@angular/core';
import { User } from '../user-model';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.css']
})
export class AuthFormComponent implements OnInit {
  @Input() currentuser: User
  constructor(
    private router: Router,
    private userService: UserService
  ){

  }

  ngOnInit() {
    
  }

  onSubmit(){
    if(this.currentuser){
      this.userService.createUser(this.currentuser).subscribe(() =>  this.router.navigate([`/pokemons`]))
    }
    console.log('The form has been submited!')

  }
}
