import { Component, OnInit } from '@angular/core';
import { User } from '../user-model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: []
})
export class RegisterComponent implements OnInit{
  newUser: User
  constructor(){

  }

  ngOnInit() {
    this.addNewUser()
  }

  addNewUser(){
    this.newUser = new User()
  }
}
