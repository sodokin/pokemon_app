import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { UserService } from './user.service';
import { UsersListComponent } from './users-list/users-list.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { AuthFormComponent } from './auth-form/auth-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { AuthGuard } from '../auth.guard';


const AuthenticationRoutes: Routes = [
  {path: 'pokemons/authentication/login', component: LoginComponent},
  {path: 'pokemons/authentication/register', component: RegisterComponent},
  {path: 'pokemons/authentication/users', component: UsersListComponent, canActivate: [AuthGuard]},
  // {path: 'authentication/details/:id', component: },
  // {path: 'authentication/update/:id', component: },

]

@NgModule({
  declarations: [
    RegisterComponent,
    LoginComponent,
    UsersListComponent,
    NavBarComponent,
    AuthFormComponent,
    LoginFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(AuthenticationRoutes)

  ],
  providers: [UserService]

})
export class AppauthenticationModule { }
