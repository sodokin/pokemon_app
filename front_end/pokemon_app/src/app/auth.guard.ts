import { Injectable, inject } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from './appauthentication/token.service';
// import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
// import { Observable } from 'rxjs';

export const AuthGuard = () => {
  const authService = inject(TokenService)
  const router = inject(Router)

  if (authService.isLoggedIn()) {
    return true
  }
  router.navigate(['pokemons/authentication/login'])
  return false
}

// @Injectable({
//   providedIn: 'root'
// })
// export class AuthGuard implements CanActivate {

//   canActivate(
//     route: ActivatedRouteSnapshot,
//     state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
//     return true;
//   }
  
// }
